import Card from './index';

import { render, screen } from '@testing-library/react';

test('Card renders closed initially', () => {
  const cardTitle = `Title_${Math.random().toString().slice()}`;
  const cardDescription = `Description_${Math.random().toString().slice()}`;
  // Title
  const titleElement = screen.queryByText(cardTitle);
  expect(titleElement).not.toBeInTheDocument();

  // Description
  const descriptionElement = screen.queryByText(cardDescription);
  expect(descriptionElement).not.toBeInTheDocument();
});

test('Card can be opened by clicking the arrow', () => {
  const cardTitle = `Test_${Math.random().toString().slice(2)}`;
  const cardDescription = `Description_${Math.random().toString().slice(2)}`;
  const cardElement = render(<Card title={cardTitle}> {cardDescription} </Card>);

  // Up Arrow:
  const upArrowElement = cardElement.getByTestId('UpArrow');
  expect(upArrowElement).toBeInTheDocument();

  // Description
  const descriptionElement = screen.queryByText(cardDescription);
  expect(descriptionElement).not.toBeInTheDocument();

});
