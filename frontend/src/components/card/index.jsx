import { Component } from 'react';
import { UpOutlined } from '@ant-design/icons';

import "./card.css";

class Card extends Component {

    constructor(props) {
        super(props);
        this.state = { expanded: false };
    }

    toggleExpand = () => this.setState(prevState => ({ expanded : !prevState.expanded }))


    render() {
        const {expanded} = this.state;
        const {title, children} = this.props;

        return (
            <div className="card">
                <div className="card-title">
                    <span>{title && title}</span>
                    <UpOutlined data-testid="UpArrow" onClick={this.toggleExpand} className={expanded ? "rotate-arrow-down" : "rotate-arrow-up"}/>
                </div>
                { expanded && (<div className="card-info">
                    <p>{children}</p>
                </div>) }
            </div>
        )
    }

}

export default Card;
