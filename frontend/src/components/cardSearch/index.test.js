import CardSearch from './index';

import { render } from '@testing-library/react';

it('Input field should be filtered correctly', () => {
  const CARDS = ['Card #1', 'Card #2', 'Alice', 'Bob'].map(title => ({title, description: Math.random().toString(36).slice(2)}));
  const searchCardElement = render(<CardSearch cards={CARDS} />)

  // Search Input
  const searchInp = searchCardElement.getByTestId("search-card")
  expect(searchInp).toBeInTheDocument();
});
