import { createRef, Component } from 'react';
import { Input } from 'antd';

import Card from '../card';
import './style.css';

const filterCards = (cards, query) => {
    if (!query) {
        return cards;
    }

    return cards.filter((card) => {
        const cardName = card.title;
        return cardName.toUpperCase().includes(query.toUpperCase())
    });
};

class CardSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            search: '',
        };
        this.inp = createRef();
        this.focusTextInput = this.focusTextInput.bind(this);
    }

    componentDidMount() {
        this.focusTextInput()
    }

    focusTextInput() {
        this.inp.current.focus();
    }

    searchChange = (event) => {
        this.setState({ search: event.target.value });
    }

    render() {
        const displayedCards = this.props.cards;
        const {search} = this.state;
        const filteredPosts = filterCards(displayedCards, search);

        return (
            <div className="card-container">
                <div className="card-container-searchbar">
                    <Input data-testid="search-card" placeholder="Filter Cards" onChange={this.searchChange} ref={this.inp} value={search}/>
                </div>
                <div className="card-container-displayarea" >
                    {filteredPosts.length < 1 ? 'Result not found!' : filteredPosts.map((card, i) => (<Card key={i} title={card.title}><span>{card.description}</span></Card>))}
                </div>
            </div>
        );
    }
}

export default CardSearch;
