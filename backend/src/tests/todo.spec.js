const PORT = process.env.PORT || 6766;

const  chai = require('chai');
const  chaiHttp = require('chai-http');

const  request = require('request');
const  expect  = require('chai').expect;
const  assert = chai.assert;
chai.use(require('chai-json-schema-ajv'));

const rules = {
    type: "object",
    properties: {
        title: {type: "string"},
        created: {type: "string", format: "date-time"}
    },
    required: ["title"],
    additionalProperties: false,
};


chai.should();
chai.use(chaiHttp);

/**
 * Test the GET ( by id ) route Todo
 */

describe("Task APIs", () => {
    describe("Test GET route /todo/:number", () => {

        it('Main page content', (done) => {
            const id = 8
            request(`http://localhost:${PORT}/todo/${id}` , (error, response, body) => {
                if(!Number.isInteger(id)) {
                    throw new TypeError('Id must be integer');
                }
                done()
            });
        });
    })
})

/**
 * Test the POST route Todo
 */

describe("Task APIs", () => {
    describe("Test POST route /todo", () => {

        it('Main page content', (done) => {
            request(`http://localhost:${PORT}/todo` , (error, response, body) => {
                expect(rules, body).to.be.validJsonSchema
                expect({ type: '__invalid__' }, body).to.not.be.validJsonSchema

                assert.validJsonSchema(rules, body)
                assert.notValidJsonSchema({ type: '__invalid__' }, body)

                done()
            })
        });
    })
})


