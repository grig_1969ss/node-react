const PORT = process.env.PORT || 6766;

const  chai = require('chai');
const  chaiHttp = require('chai-http');
const  request = require('request');
const  expect  = require('chai').expect;

// Configure chai
chai.should();
chai.use(chaiHttp);

/**
* Test the GET ( by number ) route fizzBuzz
*/

describe("Task APIs", () => {
    describe("Test GET route /fizzBuzz/:number", () => {

        it('Main page content', (done) => {
            const number = Math.floor(Math.random() * 100) + 1 || 1;
            request(`http://localhost:${PORT}/fizzBuzz/${number}` , (error, response, body) => {
                if (number % 15 === 0) expect(body).to.equal("FizzBuzz");
                else if (number % 3 === 0) expect(body).to.equal("Fizz");
                else if (number % 5 === 0) expect(body).to.equal("Buzz");
                else console.log(number);
                done();
            });
        });
    })
})
