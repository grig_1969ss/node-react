const { Router } = require('express');

const FizzBuzzRouter = Router({ mergeParams: true });

FizzBuzzRouter.get('/:number', (req, res) => {
    const { number } = req.params;
    const num = Number(number)

    if (num % 15 === 0) return res.send("FizzBuzz")
    else if (num % 3 === 0) return res.send("Fizz")
    else if (num % 5 === 0) return res.send("Buzz")
    else return res.json({number: num});
});


module.exports = FizzBuzzRouter;
