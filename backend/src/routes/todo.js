const { Router } = require('express');
const { addTodo, getTodoById } = require('../dataStore');
const Ajv = require('ajv')

const ajv = new Ajv()

const TodoRoute = Router({ mergeParams: true });

TodoRoute.get('/:id', (req, res) => {
    const { id } = req.params;

    return res.send(getTodoById(Number(id)));
});

TodoRoute.post('/', function (req,res) {
    const rules = {
        type: "object",
        properties: {
            title: {type: "string"},
            created: {type: "string", format: "date-time"}
        },
        required: ["title"],
        additionalProperties: false,
    };

    const valid = ajv.validate(rules, req.body);

    if (!valid) {
        return res.send({err: ajv.errorsText(), error: ajv.errors});
    } else {
        addTodo(req.body)
        res.send("Todo created !")
    }
});



module.exports = TodoRoute;
